var express = require('express');
var bodyParser = require('body-parser');
var Verify = require('./verify');
var Favorites = require('../models/favorites');

var favoriteRouter = express.Router();
favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')
  .all(Verify.verifyOrdinaryUser)

  .get(function (req, res, next) {
    Favorites.find({'postedBy': req.decoded._id})
      .populate('postedBy')
      .populate('dishes')
      .exec(function (err, favorite) {
        if (err) return next(err);
        res.json(favorite);
      });
  })

  .post(function (req, res, next) {
    Favorites.findOneAndUpdate({postedBy: req.decoded._id},
    {
      $addToSet: {
        dishes: req.body
      }
    },
    {
      upsert: true,
      new: true
    },
    function (err, favorite) {
      if (err) return next(err);
      res.json(favorite);
    });
  })

  .delete(function (req, res, next) {
    Favorites.findOneAndRemove({postedBy: req.decoded._id}, function (err, resp) {
      if (err) return next(err);
      res.json(resp);
    });
  });

favoriteRouter.route('/:dishId')
  .delete(Verify.verifyOrdinaryUser, function (req, res, next) {
    Favorites.findOneAndUpdate({'postedBy': req.decoded._id}, function (err, favorite) {
      if (err) return err;
      favorite.dishes.remove(req.params.dishId);
      favorite.save(function (err, resp) {
        if (err) return next(err);
        console.log(err);
        res.json(resp);
      });
    });
  });

module.exports = favoriteRouter;
